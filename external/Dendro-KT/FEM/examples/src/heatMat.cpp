//
// Created by milinda on 11/21/18.
//

#include "heatMat.h"
#include "mathUtils.h"

namespace HeatEq
{

template <unsigned int dim, int eleOrder>
HeatMat<dim,eleOrder>::HeatMat(ot::DA<dim>* da, const std::vector<ot::TreeNode<unsigned int, dim>> *octList, unsigned int dof) : feMatrix<HeatMat<dim,eleOrder>,dim>(da,octList, dof)
{
    static const unsigned int nPe=m_uiOctDA->getNumNodesPerElement();
    imV1=new double[nPe];
    imV2=new double[nPe];

    Qx=new double[nPe];
    Qy=new double[nPe];
    Qz=new double[nPe];
#ifdef PROFILE
    timers.fill(0.0);
#endif

}

template <unsigned int dim, int eleOrder>
HeatMat<dim,eleOrder>::~HeatMat()
{

    delete [] imV1;
    delete [] imV2;

    delete [] Qx;
    delete [] Qy;
    delete [] Qz;

    imV1=NULL;
    imV2=NULL;

    Qx=NULL;
    Qy=NULL;
    Qz=NULL;
#ifdef PROFILE

  static constexpr int p4=intPow(eleOrder+1, dim);
    unsigned int totalFLOPS[OP::MAX];
    static constexpr int nPe=intPow(eleOrder+1, dim);
    totalFLOPS[OP::KZ] = 3* (2 * eleOrder + 1) * p4;
    totalFLOPS[OP::KY] = 3* (2 * eleOrder + 1) * p4;
    totalFLOPS[OP::KX] = 3* (2 * eleOrder + 1) * p4;
    totalFLOPS[OP::KZT] = 3* (2 * eleOrder + 1) * p4;
    totalFLOPS[OP::KYT] = 3 *(2 * eleOrder + 1) * p4;
    totalFLOPS[OP::KXT] = 3*(2 * eleOrder + 1) * p4;
    totalFLOPS[OP::WEIGHT] = 2*nPe;
    totalFLOPS[OP::SCALE] =  6*nPe;
    totalFLOPS[OP::LAST] =  2*nPe;

    std::cout << "Total number of iter = " << iter<<"\n";
    unsigned int totalGPFLOPS = 0;
    double totalTime = 0;
    for(int i = 0; i < OP::MAX; i++){
       double GFLOPS = totalFLOPS[i]*iter/timers[i];
       totalGPFLOPS += totalFLOPS[i];
       totalTime += timers[i];
       std::cout << opNames[i] << " " << GFLOPS/1E9 << "\n";
    }
    std::cout << "Total FLOPS = " << totalGPFLOPS*iter/totalTime/1E9 << "\n";
#endif

//    std::cout << "Time = " << totalSec << "\n";
//
//
//    unsigned int totalFLOPS = 2*dim*temp+4*nPe /** wights**/ + 2*nPe; /** (weight *jac*value**/;
//    double GFLOPS = totalFLOPS*m_uiOctDA->getLocalElementSz()/totalSec;


}

template <unsigned int dim, int eleOrder>
void HeatMat<dim,eleOrder>::elementalMatVec(const VECType* in,VECType* out, unsigned int ndofs, const double*coords,double scale, bool isElementBoundary)
{

//#pragma prefetch imV1 imV2 Qx Qy Qz
  //TODO use ndofs
    const RefElement* refEl=m_uiOctDA->getReferenceElement();
    static constexpr TensorImplementation implementation = TensorImplementation::UNROLLED; /// Giving best performance

    const double * Q1d=refEl->getQ1d();
    const double * QT1d=refEl->getQT1d();
    const double * Dg=refEl->getDg1d();
    const double * DgT=refEl->getDgT1d();
    const double * W1d=refEl->getWgq();


    static constexpr int nPe=intPow(eleOrder+1, dim);
    static constexpr int nrp=eleOrder+1;

    const double szX = (coords[(nPe-1)*m_uiDim] - coords[0])/(m_uiPtMax.x() -m_uiPtMin.x()) ;

   double refElSz=refEl->getElementSz();

    const double Jx = 1.0/(refElSz/(double (szX)));

#ifdef PROFILE
    iter++;
    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
#endif
#pragma prefetch Qx
    //x derivative
    DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(Dg,in,imV1);
    DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(Q1d,imV1,imV2);
    DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(Q1d,imV2,Qx);
#ifdef PROFILE
   std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch Qy
    //y derivative
    DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(Q1d,in,imV2);

  //  std::memcpy(Qx,imV1,sizeof(double)*nPe);
    DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(Dg,imV2,imV1);
    DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(Q1d,imV1,Qy);
#ifdef PROFILE
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch Qz
    //z derivative
    DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(Q1d,in,imV1);
    DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(Q1d,imV1,imV2);
    DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(Dg,imV2,Qz);
#ifdef PROFILE
    std::chrono::high_resolution_clock::time_point t3 = std::chrono::high_resolution_clock::now();
    //std::cout<<"Stifness:  elem: "<<elem<<" ele Sz: "<<(elem.maxX()-elem.minX())<<" szX: "<<szX<<" Jx: "<<Jx<<" J: "<<(Jx*Jy*Jz)<<std::endl;
#endif

  for( int k=0;k<nrp;k++) {
    for ( int j = 0; j < nrp; j++) {
      double & temp = refElSz;
    temp = W1d[k]*W1d[j];
#pragma vector always
      for (int i = 0; i < nrp; i++) {
        imV1[k * nrp * nrp + j * nrp + i] = W1d[i] * temp;
      }
    }
  }
#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t4 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch Qy

#pragma vector always
  for(int i = 0; i < nPe; i++) {
    Qz[i] *= Jx*imV1[i];
  }
#pragma prefetch Qx
#pragma vector always
  for(int i = 0; i < nPe; i++) {
    Qy[i] *= Jx * imV1[i];
  }

#pragma vector always
  for(int i = 0; i < nPe; i++) {
    Qx[i] *= Jx * imV1[i];
  }
#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t5 = std::chrono::high_resolution_clock::now();
#endif

//  for(unsigned int k=0;k<(eleOrder+1);k++)
//        for(unsigned int j=0;j<(eleOrder+1);j++)
//            for(unsigned int i=0;i<(eleOrder+1);i++)
//            {
//                Qx[k*(eleOrder+1)*(eleOrder+1)+j*(eleOrder+1)+i]*=( ((Jy*Jz)/Jx)*W1d[i]*W1d[j]*W1d[k]);
//                Qy[k*(eleOrder+1)*(eleOrder+1)+j*(eleOrder+1)+i]*=( ((Jx*Jz)/Jy)*W1d[i]*W1d[j]*W1d[k]);
//                Qz[k*(eleOrder+1)*(eleOrder+1)+j*(eleOrder+1)+i]*=( ((Jx*Jy)/Jz)*W1d[i]*W1d[j]*W1d[k]);
//            }

#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t6 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch Qy
  DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(DgT,Qx,imV1);
  DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(QT1d,imV1,imV2);
  DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(QT1d,imV2,Qx);
#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t7 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch Qz
    DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(QT1d,Qy,imV2);
    DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(DgT,imV2,imV1);
    DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(QT1d,imV1,Qy);
#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t8 = std::chrono::high_resolution_clock::now();
#endif

#pragma prefetch out
  DENDRO_TENSOR_IIAX_APPLY_ELEM<nrp,implementation>(QT1d,Qz,imV1);
  DENDRO_TENSOR_IAIX_APPLY_ELEM<nrp,implementation>(QT1d,imV1,imV2);
  DENDRO_TENSOR_AIIX_APPLY_ELEM<nrp,implementation>(DgT,imV2,Qz);
#ifdef PROFILE
  std::chrono::high_resolution_clock::time_point t9 = std::chrono::high_resolution_clock::now();
#endif

#pragma vector always
    for(int i=0;i<nPe;i++) {
      out[i] = Qx[i]+Qy[i]+ Qz[i];
    }

#ifdef PROFILE
   std::chrono::high_resolution_clock::time_point t10 = std::chrono::high_resolution_clock::now();
    timers[OP::KZ] += std::chrono::duration<double>(t1 - t0).count();
    timers[OP::KY] += std::chrono::duration<double>(t2 - t1).count();
    timers[OP::KX] += std::chrono::duration<double>(t3 - t2).count();
    timers[OP::WEIGHT] += std::chrono::duration<double>(t4 - t3).count();
    timers[OP::SCALE] += std::chrono::duration<double>(t5 - t4).count();
    timers[OP::KZT] += std::chrono::duration<double>(t7 - t6).count();
    timers[OP::KYT] += std::chrono::duration<double>(t8 - t7).count();
    timers[OP::KXT] += std::chrono::duration<double>(t9 - t8).count();
    timers[OP::LAST] += std::chrono::duration<double>(t10 - t9).count();
#endif
}

template <unsigned int dim, int eleOrder>
bool HeatMat<dim,eleOrder>::preMatVec(const VECType* in,VECType* out,double scale)
{
    // apply boundary conditions.
    std::vector<size_t> bdyIndex;
    m_uiOctDA->getBoundaryNodeIndices(bdyIndex);

    for(unsigned int i=0;i<bdyIndex.size();i++)
        out[bdyIndex[i]]=0.0;

    return true;
}

template <unsigned int dim, int eleOrder>
bool HeatMat<dim,eleOrder>::postMatVec(const VECType* in,VECType* out,double scale) {

    // apply boundary conditions.
    std::vector<size_t> bdyIndex;
    m_uiOctDA->getBoundaryNodeIndices(bdyIndex);

    for(unsigned int i=0;i<bdyIndex.size();i++)
        out[bdyIndex[i]]=0.0;

    return true;
}


template <unsigned int dim, int eleOrder>
double HeatMat<dim,eleOrder>::gridX_to_X(double x) const
{
    double Rg_x=1.0;
    return (((x)/(Rg_x))*((m_uiPtMax.x()-m_uiPtMin.x()))+m_uiPtMin.x());
}

template <unsigned int dim, int eleOrder>
double HeatMat<dim,eleOrder>::gridY_to_Y(double y) const
{
    double Rg_y=1.0;
    return (((y)/(Rg_y))*((m_uiPtMax.y()-m_uiPtMin.y()))+m_uiPtMin.y());
}


template <unsigned int dim, int eleOrder>
double HeatMat<dim,eleOrder>::gridZ_to_Z(double z) const
{
    double Rg_z=1.0;
    return (((z)/(Rg_z))*((m_uiPtMax.z()-m_uiPtMin.z()))+m_uiPtMin.z());
}

template <unsigned int dim, int eleOrder>
int HeatMat<dim,eleOrder>::cgSolve(double * x ,double * b,int max_iter, double& tol,unsigned int var)
{
    double resid,alpha,beta,rho,rho_1;
    int status=1; // 0 indicates it has solved the system within the specified max_iter, 1 otherwise.

    const unsigned int local_dof=m_uiOctDA->getLocalNodalSz();

    MPI_Comm globalComm=m_uiOctDA->getGlobalComm();

    if(m_uiOctDA->isActive())
    {

        int activeRank=m_uiOctDA->getRankActive();
        int activeNpes=m_uiOctDA->getNpesActive();

        MPI_Comm activeComm=m_uiOctDA->getCommActive();

        double* p;
        double* z;
        double* q;
        double* Ax;
        double* Ap;
        double* r0;
        double* r1;

        m_uiOctDA->createVector(p);
        m_uiOctDA->createVector(z);
        m_uiOctDA->createVector(q);

        m_uiOctDA->createVector(Ax);
        m_uiOctDA->createVector(Ap);
        m_uiOctDA->createVector(r0);
        m_uiOctDA->createVector(r1);

        double normb = normLInfty(b,local_dof,activeComm);
        par::Mpi_Bcast(&normb,1,0,activeComm);

        if(!activeRank)
            std::cout<<"normb = "<<normb<<std::endl;

        this->matVec(x,Ax);

        /*char fPrefix[256];
        sprintf(fPrefix,"%s_%d","cg",0);
        const char * varNames[]={"U"};
        const double * var[]={Ax};
        io::vtk::mesh2vtuFine(mesh,fPrefix,0,NULL,NULL,1,varNames,var);
*/
        for(unsigned int i=0;i<local_dof;i++)
        {
            r0[i]=b[i]-Ax[i];
            p[i]=r0[i];
        }


        if (normb == 0.0)
            normb = 1;

        double normr=normLInfty(r0,local_dof,activeComm);
        par::Mpi_Bcast(&normr,1,0,activeComm);
        if(!activeRank) std::cout<<"initial residual : "<<(normr/normb)<<std::endl;

        if ((resid = normr / normb) <= tol) {
            tol = resid;
            max_iter = 0;

            m_uiOctDA->destroyVector(p);
            m_uiOctDA->destroyVector(z);
            m_uiOctDA->destroyVector(q);

            m_uiOctDA->destroyVector(Ax);
            m_uiOctDA->destroyVector(Ap);
            m_uiOctDA->destroyVector(r0);
            m_uiOctDA->destroyVector(r1);

            status=0;
        }

        if(status!=0)
        {

            for(unsigned int i=1;i<=max_iter;i++)
            {

                this->matVec(p,Ap);

                alpha=(dot(r0,r0,local_dof,activeComm)/dot(p,Ap,local_dof,activeComm));
                par::Mpi_Bcast(&alpha,1,0,activeComm);

                //if(!activeRank) std::cout<<"rank: " <<activeRank<<" alpha: "<<alpha<<std::endl;
                for(unsigned int e=0;e<local_dof;e++)
                {
                    x[e]+=alpha*p[e];
                    r1[e]=r0[e]-alpha*Ap[e];
                }

                normr=normLInfty(r1,local_dof,activeComm);
                par::Mpi_Bcast(&normr,1,0,activeComm);

                if((!activeRank) && (i%10==0)) std::cout<<" iteration : "<<i<<" residual : "<<resid<<std::endl;

                if ((resid = normr / normb) <= tol) {

                    if((!activeRank)) std::cout<<" iteration : "<<i<<" residual : "<<resid<<std::endl;
                    tol = resid;
                    m_uiOctDA->destroyVector(p);
                    m_uiOctDA->destroyVector(z);
                    m_uiOctDA->destroyVector(q);

                    m_uiOctDA->destroyVector(Ax);
                    m_uiOctDA->destroyVector(Ap);
                    m_uiOctDA->destroyVector(r0);
                    m_uiOctDA->destroyVector(r1);

                    status=0;
                    break;
                }

                beta=(dot(r1,r1,local_dof,activeComm)/dot(r0,r0,local_dof,activeComm));
                par::Mpi_Bcast(&beta,1,0,activeComm);

                //if(!activeRank) std::cout<<"<r_1,r_1> : "<<dot(r1+nodeLocalBegin,r1+nodeLocalBegin,local_dof,activeComm)<<" <r_0,r_0>: "<<dot(r0+nodeLocalBegin,r0+nodeLocalBegin,local_dof,activeComm)<<" beta "<<beta<<std::endl;



                for(unsigned int e=0;e<local_dof;e++)
                {
                    p[e]=r1[e]+beta*p[e];
                    r0[e]=r1[e];
                }


            }

            if(status!=0)
            {
                tol = resid;
                m_uiOctDA->destroyVector(p);
                m_uiOctDA->destroyVector(z);
                m_uiOctDA->destroyVector(q);

                m_uiOctDA->destroyVector(Ax);
                m_uiOctDA->destroyVector(Ap);
                m_uiOctDA->destroyVector(r0);
                m_uiOctDA->destroyVector(r1);
                status=1;

            }



        }


    }


    // bcast act as a barrier for active and inactive meshes.
    par::Mpi_Bcast(&tol,1,0,globalComm);
    return status;
}

// Template instantiations.
//template class HeatMat<2u>;
template class HeatMat<3u,1>;
template class HeatMat<3u,2>;
//template class HeatMat<4u>;


}//namespace HeatEq
