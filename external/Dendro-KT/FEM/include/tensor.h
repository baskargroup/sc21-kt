//
// Created by milinda on 1/15/17.
//

/**
 *
 * @author Milinda Fernando
 * @author Masado Ishii
 * @breif contains the utilities for tensor kronecker products for interpolations.
 *
 * */

#ifndef SFCSORTBENCH_DENDROTENSOR_H
#define SFCSORTBENCH_DENDROTENSOR_H


#include "mathUtils.h"
enum TensorImplementation:int{
  DEFAULT = 0,
  VECTORIZED = 1,
  UNROLLED = 2,
  MAX = 3
};

// TODO make a new namespace



/**
 * @tparam dim Dimension of element, i.e. order of tensor.
 * @tparam da  Datatype of vectors.
 * @tparam forward If true, axes are evaluated in increasing order.
 * @param [in] M Size of tensor in 1D.
 * @param [in] A Array of pointers to interpolation matrices, ordered by axis.
 * @param [in] in Array of pointers to input buffers, ordered with source in position 0.
 * @param [in] out Array of pointers to output buffers, ordered with destination in position (dim-1).
 * @param ndofs Number of degrees of freedom in the vector, e.g. 3 for xyzxyz.
 */
template <unsigned int dim, typename da, bool forward>
void KroneckerProduct(unsigned M, const da **A, const da **in, da **out, unsigned int ndofs);


/**
 * @brief Forall (i,j,k), Q_ijk *= A * P_ijk, where P_ijk == W_i * W_j * W_k;
 * @tparam dim Order of the tensor.
 * @tparam T Component type.
 *
 * Generalizes the following nested for-loop to any nesting level:
 *   int idx = 0;
 *   for (int k = 0; k < l; k++)
 *   {
 *     const double a2 = A*W[k];
 *     for (int j = 0; j < l; j++)
 *     {
 *       const double a1 = a2*W[j];
 *       for (int i = 0; i < l; i++)
 *       {
 *         const double a0 = a1*W[i];
 *         Q[idx++] *= a0;
 *       }
 *     }
 *   }
 */
template <typename T, unsigned int dim>
struct SymmetricOuterProduct
{
  inline static void applyHadamardProduct(unsigned int length1d, T *Q, const T *W1d, const T premult = 1);
};

template <typename T, unsigned int dim>
inline void SymmetricOuterProduct<T,dim>::applyHadamardProduct(
    unsigned int length1d, T *Q, const T *W1d, const T premult)
{
  const unsigned int stride = intPow(length1d, dim-1);
  for (unsigned int ii = 0; ii < length1d; ii++)
    SymmetricOuterProduct<T, dim-1>::applyHadamardProduct(length1d, &Q[stride * ii], W1d, premult * W1d[ii]);
}

template <typename T>
struct SymmetricOuterProduct<T, 1>
{
  inline static void applyHadamardProduct(unsigned int length1d, T *Q, const T *W1d, const T premult = 1)
  {
    for (unsigned int ii = 0; ii < length1d; ii++)
      Q[ii] *= premult * W1d[ii];
  }
};




/** Apply the 1D interpolation for the input vector x and output the interpolated values in the vector Y.
 *
 *
 *
 * \param [in]  M  size of the vector
 * \param [in]  A  interpolation matrix
 * \param [in]  X  input data for the interpolation
 * \param [out] Y  interpolated values.
 *

 */
void DENDRO_TENSOR_AIIX_APPLY_ELEM (const int M, const double*  A, const double*  X, double*  Y);

template<int M, TensorImplementation implementation>
void DENDRO_TENSOR_AIIX_APPLY_ELEM (const double* __restrict__  A, const double* __restrict__  X, double*  Y) {
  if(implementation == TensorImplementation::VECTORIZED) {
    /// Total Flops = (m^3+2*m^4)
    int i, j, k;
    double d, e;
    for (i = 0; i < M; ++i) {
      d = A[i];
#pragma vector always
      for (j = 0; j < M * M; ++j) {
        Y[i * M * M + j] = d * X[j];
      }
      for (k = 1; k < M; ++k) {
        d = A[i + k * M];
#pragma vector always
        for (j = 0; j < M * M; ++j) {
          e = d * X[M * M * k + j];
          Y[i * M * M + j] += e;
        }
      }
    }
  }
  else if(implementation == TensorImplementation::UNROLLED){
    if(M == 3) {
      Y[0] = A[1 - 1] * X[1 - 1] + A[4 - 1] * X[10 - 1] + A[7 - 1] * X[19 - 1];
      Y[1] = A[1 - 1] * X[2 - 1] + A[4 - 1] * X[11 - 1] + A[7 - 1] * X[20 - 1];
      Y[2] = A[1 - 1] * X[3 - 1] + A[4 - 1] * X[12 - 1] + A[7 - 1] * X[21 - 1];
      Y[3] = A[1 - 1] * X[4 - 1] + A[4 - 1] * X[13 - 1] + A[7 - 1] * X[22 - 1];
      Y[4] = A[1 - 1] * X[5 - 1] + A[4 - 1] * X[14 - 1] + A[7 - 1] * X[23 - 1];
      Y[5] = A[1 - 1] * X[6 - 1] + A[4 - 1] * X[15 - 1] + A[7 - 1] * X[24 - 1];
      Y[6] = A[1 - 1] * X[7 - 1] + A[4 - 1] * X[16 - 1] + A[7 - 1] * X[25 - 1];
      Y[7] = A[1 - 1] * X[8 - 1] + A[4 - 1] * X[17 - 1] + A[7 - 1] * X[26 - 1];
      Y[8] = A[1 - 1] * X[9 - 1] + A[4 - 1] * X[18 - 1] + A[7 - 1] * X[27 - 1];
      Y[9] = A[2 - 1] * X[1 - 1] + A[5 - 1] * X[10 - 1] + A[8 - 1] * X[19 - 1];
      Y[10] = A[2 - 1] * X[2 - 1] + A[5 - 1] * X[11 - 1] + A[8 - 1] * X[20 - 1];
      Y[11] = A[2 - 1] * X[3 - 1] + A[5 - 1] * X[12 - 1] + A[8 - 1] * X[21 - 1];
      Y[12] = A[2 - 1] * X[4 - 1] + A[5 - 1] * X[13 - 1] + A[8 - 1] * X[22 - 1];
      Y[13] = A[2 - 1] * X[5 - 1] + A[5 - 1] * X[14 - 1] + A[8 - 1] * X[23 - 1];
      Y[14] = A[2 - 1] * X[6 - 1] + A[5 - 1] * X[15 - 1] + A[8 - 1] * X[24 - 1];
      Y[15] = A[2 - 1] * X[7 - 1] + A[5 - 1] * X[16 - 1] + A[8 - 1] * X[25 - 1];
      Y[16] = A[2 - 1] * X[8 - 1] + A[5 - 1] * X[17 - 1] + A[8 - 1] * X[26 - 1];
      Y[17] = A[2 - 1] * X[9 - 1] + A[5 - 1] * X[18 - 1] + A[8 - 1] * X[27 - 1];
      Y[18] = A[3 - 1] * X[1 - 1] + A[6 - 1] * X[10 - 1] + A[9 - 1] * X[19 - 1];
      Y[19] = A[3 - 1] * X[2 - 1] + A[6 - 1] * X[11 - 1] + A[9 - 1] * X[20 - 1];
      Y[20] = A[3 - 1] * X[3 - 1] + A[6 - 1] * X[12 - 1] + A[9 - 1] * X[21 - 1];
      Y[21] = A[3 - 1] * X[4 - 1] + A[6 - 1] * X[13 - 1] + A[9 - 1] * X[22 - 1];
      Y[22] = A[3 - 1] * X[5 - 1] + A[6 - 1] * X[14 - 1] + A[9 - 1] * X[23 - 1];
      Y[23] = A[3 - 1] * X[6 - 1] + A[6 - 1] * X[15 - 1] + A[9 - 1] * X[24 - 1];
      Y[24] = A[3 - 1] * X[7 - 1] + A[6 - 1] * X[16 - 1] + A[9 - 1] * X[25 - 1];
      Y[25] = A[3 - 1] * X[8 - 1] + A[6 - 1] * X[17 - 1] + A[9 - 1] * X[26 - 1];
      Y[26] = A[3 - 1] * X[9 - 1] + A[6 - 1] * X[18 - 1] + A[9 - 1] * X[27 - 1];
    }
    else if(M == 2){
      Y[0] = A[1-1]*X[1-1]+A[3-1]*X[5-1];
      Y[1] = A[1-1]*X[2-1]+A[3-1]*X[6-1];
      Y[2] = A[1-1]*X[3-1]+A[3-1]*X[7-1];
      Y[3] = A[1-1]*X[4-1]+A[3-1]*X[8-1];
      Y[4] = A[2-1]*X[1-1]+A[4-1]*X[5-1];
      Y[5] = A[2-1]*X[2-1]+A[4-1]*X[6-1];
      Y[6] = A[2-1]*X[3-1]+A[4-1]*X[7-1];
      Y[7] = A[2-1]*X[4-1]+A[4-1]*X[8-1];
    } else{
      throw std::runtime_error("Not supprorted");
    }
  }
  else if(implementation == TensorImplementation::DEFAULT){
    DENDRO_TENSOR_AIIX_APPLY_ELEM(M,A,X,Y);
  }
}


/** Apply the 1D interpolation for the input vector x and output the interpolated values in the vector Y.
 *
 *
 *
 * \param [in]  M  size of the vector
 * \param [in]  A  interpolation matrix
 * \param [in]  X  input data for the interpolation
 * \param [out] Y  interpolated values.
 *

 */
void DENDRO_TENSOR_IIAX_APPLY_ELEM(const int M, const double*  A, const double*  X, double*  Y);

template<int M,TensorImplementation implementation>
void DENDRO_TENSOR_IIAX_APPLY_ELEM(const double * __restrict__ A, const double * __restrict__  X, double *Y) {
/// TOtal flops = 2*m^4
if(implementation == TensorImplementation::VECTORIZED) {
  int i, j, k;
  double e;
  for (i = 0; i < M * M; i++) {
    // _mm_prefetch( (const char *)(X + (i+10)*M), 2);
    //_mm_prefetch( (const char *)(Y + (i+10)*M), 2);
    for (j = 0; j < M; ++j) {
      e = 0;
//#pragma vector always
      for (k = 0; k < M; ++k) {
        e += X[i * M + k] * A[k * M + j];
      }
      Y[i * M + j] = e;
    }
  }
} else if(implementation == TensorImplementation::UNROLLED){
  if(M == 3){
  Y[0]  = A[1 - 1]*X[1 -1]+A[4-1]*X[2-1] +A[7-1]*X[3-1];
  Y[1]  = A[2 - 1]*X[1 -1]+A[5-1]*X[2-1] +A[8-1]*X[3-1];
  Y[2]  = A[3 - 1]*X[1 -1]+A[6-1]*X[2-1] +A[9-1]*X[3-1];
  Y[3]  = A[1 - 1]*X[4 -1]+A[4-1]*X[5-1] +A[7-1]*X[6-1];
  Y[4]  = A[2 - 1]*X[4 -1]+A[5-1]*X[5-1] +A[8-1]*X[6-1];
  Y[5]  = A[3 - 1]*X[4 -1]+A[6-1]*X[5-1] +A[9-1]*X[6-1];
  Y[6]  = A[1 - 1]*X[7 -1]+A[4-1]*X[8-1] +A[7-1]*X[9-1];
  Y[7]  = A[2 - 1]*X[7 -1]+A[5-1]*X[8-1] +A[8-1]*X[9-1];
  Y[8]  = A[3 - 1]*X[7 -1]+A[6-1]*X[8-1] +A[9-1]*X[9-1];
  Y[9]  = A[1 - 1]*X[10-1]+A[4-1]*X[11-1]+A[7-1]*X[12-1];
  Y[10] = A[2 - 1]*X[10-1]+A[5-1]*X[11-1]+A[8-1]*X[12-1];
  Y[11] = A[3 - 1]*X[10-1]+A[6-1]*X[11-1]+A[9-1]*X[12-1];
  Y[12] = A[1 - 1]*X[13-1]+A[4-1]*X[14-1]+A[7-1]*X[15-1];
  Y[13] = A[2 - 1]*X[13-1]+A[5-1]*X[14-1]+A[8-1]*X[15-1];
  Y[14] = A[3 - 1]*X[13-1]+A[6-1]*X[14-1]+A[9-1]*X[15-1];
  Y[15] = A[1 - 1]*X[16-1]+A[4-1]*X[17-1]+A[7-1]*X[18-1];
  Y[16] = A[2 - 1]*X[16-1]+A[5-1]*X[17-1]+A[8-1]*X[18-1];
  Y[17] = A[3 - 1]*X[16-1]+A[6-1]*X[17-1]+A[9-1]*X[18-1];
  Y[18] = A[1 - 1]*X[19-1]+A[4-1]*X[20-1]+A[7-1]*X[21-1];
  Y[19] = A[2 - 1]*X[19-1]+A[5-1]*X[20-1]+A[8-1]*X[21-1];
  Y[20] = A[3 - 1]*X[19-1]+A[6-1]*X[20-1]+A[9-1]*X[21-1];
  Y[21] = A[1 - 1]*X[22-1]+A[4-1]*X[23-1]+A[7-1]*X[24-1];
  Y[22] = A[2 - 1]*X[22-1]+A[5-1]*X[23-1]+A[8-1]*X[24-1];
  Y[23] = A[3 - 1]*X[22-1]+A[6-1]*X[23-1]+A[9-1]*X[24-1];
  Y[24] = A[1 - 1]*X[25-1]+A[4-1]*X[26-1]+A[7-1]*X[27-1];
  Y[25] = A[2 - 1]*X[25-1]+A[5-1]*X[26-1]+A[8-1]*X[27-1];
  Y[26] = A[3 - 1]*X[25-1]+A[6-1]*X[26-1]+A[9-1]*X[27-1];
  }
  else if(M==2){
    Y[0] = A[1 - 1]*X[1-1]+A[3-1]*X[2-1];
    Y[1] = A[2 - 1]*X[1-1]+A[4-1]*X[2-1];
    Y[2] = A[1 - 1]*X[3-1]+A[3-1]*X[4-1];
    Y[3] = A[2 - 1]*X[3-1]+A[4-1]*X[4-1];
    Y[4] = A[1 - 1]*X[5-1]+A[3-1]*X[6-1];
    Y[5] = A[2 - 1]*X[5-1]+A[4-1]*X[6-1];
    Y[6] = A[1 - 1]*X[7-1]+A[3-1]*X[8-1];
    Y[7] = A[2 - 1]*X[7-1]+A[4-1]*X[8-1];
  }
  else{
    throw std::runtime_error("Not supported");
  }
} else if(implementation == TensorImplementation::DEFAULT){
  DENDRO_TENSOR_IIAX_APPLY_ELEM(M,A,X,Y);
}

}




/** Apply the 1D interpolation for the input vector x and output the interpolated values in the vector Y.
 *
 *
 *
 * \param [in]  M  size of the vector
 * \param [in]  A  interpolation matrix
 * \param [in]  X  input data for the interpolation
 * \param [out] Y  interpolated values.
 *

 */
void DENDRO_TENSOR_IAIX_APPLY_ELEM (const int M, const double*  A, const double*  X, double*  Y);

template<int M, TensorImplementation implementation>
void DENDRO_TENSOR_IAIX_APPLY_ELEM(const double * __restrict__ A, const double * __restrict__  X, double *Y) {
/// Total Flops = (m^3 + 2m^4)
if(implementation == TensorImplementation::VECTORIZED) {
  int i, j, k, ib;
  double d, e;
  for (ib = 0; ib < M; ++ib) {
    for (i = 0; i < M; ++i) {
      d = A[i];
#pragma vector always
      for (j = 0; j < M; ++j) {
        Y[ib * M * M + i * M + j] =
          d * X[ib * M * M + j];
      }
      for (k = 1; k < M; ++k) {
        d = A[i + k * M];
#pragma vector always
        for (j = 0; j < M; ++j) {
          e = d * X[ib * M * M + M * k + j];
          Y[ib * M * M + i * M + j] += e;
        }
      }
    }
  }
}
else if(implementation == TensorImplementation::UNROLLED){
  if(M==3) {
    Y[0] = A[1] * X[1 - 1] + A[4 - 1] * X[4 - 1] + A[7 - 1] * X[7 - 1];
    Y[1] = A[1] * X[2 - 1] + A[4 - 1] * X[5 - 1] + A[7 - 1] * X[8 - 1];
    Y[2] = A[1] * X[3 - 1] + A[4 - 1] * X[6 - 1] + A[7 - 1] * X[9 - 1];
    Y[3] = A[2] * X[1 - 1] + A[5 - 1] * X[4 - 1] + A[8 - 1] * X[7 - 1];
    Y[4] = A[2] * X[2 - 1] + A[5 - 1] * X[5 - 1] + A[8 - 1] * X[8 - 1];
    Y[5] = A[2] * X[3 - 1] + A[5 - 1] * X[6 - 1] + A[8 - 1] * X[9 - 1];
    Y[6] = A[3] * X[1 - 1] + A[6 - 1] * X[4 - 1] + A[9 - 1] * X[7 - 1];
    Y[7] = A[3] * X[2 - 1] + A[6 - 1] * X[5 - 1] + A[9 - 1] * X[8 - 1];
    Y[8] = A[3] * X[3 - 1] + A[6 - 1] * X[6 - 1] + A[9 - 1] * X[9 - 1];
    Y[9] = A[1] * X[10 - 1] + A[4 - 1] * X[13 - 1] + A[7 - 1] * X[16 - 1];
    Y[10] = A[1] * X[11 - 1] + A[4 - 1] * X[14 - 1] + A[7 - 1] * X[17 - 1];
    Y[11] = A[1] * X[12 - 1] + A[4 - 1] * X[15 - 1] + A[7 - 1] * X[18 - 1];
    Y[12] = A[2] * X[10 - 1] + A[5 - 1] * X[13 - 1] + A[8 - 1] * X[16 - 1];
    Y[13] = A[2] * X[11 - 1] + A[5 - 1] * X[14 - 1] + A[8 - 1] * X[17 - 1];
    Y[14] = A[2] * X[12 - 1] + A[5 - 1] * X[15 - 1] + A[8 - 1] * X[18 - 1];
    Y[15] = A[3] * X[10 - 1] + A[6 - 1] * X[13 - 1] + A[9 - 1] * X[16 - 1];
    Y[16] = A[3] * X[11 - 1] + A[6 - 1] * X[14 - 1] + A[9 - 1] * X[17 - 1];
    Y[17] = A[3] * X[12 - 1] + A[6 - 1] * X[15 - 1] + A[9 - 1] * X[18 - 1];
    Y[18] = A[1] * X[19 - 1] + A[4 - 1] * X[22 - 1] + A[7 - 1] * X[25 - 1];
    Y[19] = A[1] * X[20 - 1] + A[4 - 1] * X[23 - 1] + A[7 - 1] * X[26 - 1];
    Y[20] = A[1] * X[21 - 1] + A[4 - 1] * X[24 - 1] + A[7 - 1] * X[27 - 1];
    Y[21] = A[2] * X[19 - 1] + A[5 - 1] * X[22 - 1] + A[8 - 1] * X[25 - 1];
    Y[22] = A[2] * X[20 - 1] + A[5 - 1] * X[23 - 1] + A[8 - 1] * X[26 - 1];
    Y[23] = A[2] * X[21 - 1] + A[5 - 1] * X[24 - 1] + A[8 - 1] * X[27 - 1];
    Y[24] = A[3] * X[19 - 1] + A[6 - 1] * X[22 - 1] + A[9 - 1] * X[25 - 1];
    Y[25] = A[3] * X[20 - 1] + A[6 - 1] * X[23 - 1] + A[9 - 1] * X[26 - 1];
    Y[26] = A[3] * X[21 - 1] + A[6 - 1] * X[24 - 1] + A[9 - 1] * X[27 - 1];
  } else if(M == 2){
    Y[0] = A[1-1]*X[1-1]+A[3-1]*X[3-1];
    Y[1] = A[1-1]*X[2-1]+A[3-1]*X[4-1];
    Y[2] = A[2-1]*X[1-1]+A[4-1]*X[3-1];
    Y[3] = A[2-1]*X[2-1]+A[4-1]*X[4-1];
    Y[4] = A[1-1]*X[5-1]+A[3-1]*X[7-1];
    Y[5] = A[1-1]*X[6-1]+A[3-1]*X[8-1];
    Y[6] = A[2-1]*X[5-1]+A[4-1]*X[7-1];
    Y[7] = A[2-1]*X[6-1]+A[4-1]*X[8-1];
  }
  else{
    throw std::runtime_error("Not supported");
  }
}else if(implementation == TensorImplementation::DEFAULT) {
  DENDRO_TENSOR_IAIX_APPLY_ELEM(M, A, X, Y);
}

}


/** Apply the 1D interpolation for the input vector x and output the interpolated values in the vector Y.
 *
 *
 *
 * \param [in]  M  size of the vector
 * \param [in]  A  interpolation matrix
 * \param [in]  X  input data for the interpolation
 * \param [out] Y  interpolated values.
 *

 */
void DENDRO_TENSOR_IAX_APPLY_ELEM_2D(const int M, const double*  A, const double*  X, double*  Y);



/** Apply the 1D interpolation for the input vector x and output the interpolated values in the vector Y.
 *
 *
 *
 * \param [in]  M  size of the vector
 * \param [in]  A  interpolation matrix
 * \param [in]  X  input data for the interpolation
 * \param [out] Y  interpolated values.
 *

 */
void DENDRO_TENSOR_AIX_APPLY_ELEM_2D (const int M, const double*  A, const double*  X, double*  Y);


#endif //SFCSORTBENCH_DENDROTENSOR_H
