//
// Created by maksbh on 6/13/20.
//

#include <Traversal/Refinement.h>
#include <DendriteUtils.h>
#include <intergridTransfer.h>

Refinement::Refinement(DA *octDA, const std::vector<TREENODE> &treePart, const DomainExtents &domainInfo)
  : Traversal(octDA, treePart, domainInfo) {
  assert(octDA->getLocalElementSz() == treePart.size());
  refineFlags_.resize(treePart.size(), ot::OCT_FLAGS::Refine::OCT_NO_CHANGE);
  m_refinementType = RefinementType::POSITION_BASED;
  coords_.resize(octDA->getNumNodesPerElement());

}

void Refinement::traverseOperation(TALYFEMLIB::FEMElm &fe) {
  assert(counter_ < refineFlags_.size());
  coordsToZeroptv(m_coords, coords_, this->m_octDA->getElementOrder(), true);
  refineFlags_[counter_] = getRefineFlags(fe, coords_);
  counter_++;
}

DA *Refinement::getRefineDA(std::vector<ot::TreeNode<DENDRITE_UINT, DIM>> &oldDAtreeNode) {
  bool doRefine = true;
  if (std::all_of(refineFlags_.begin(), refineFlags_.end(),
                  [](ot::OCT_FLAGS::Refine v) { return v == ot::OCT_FLAGS::Refine::OCT_NO_CHANGE; })) {
    doRefine = false;
  }

  bool gRefine = false;
  MPI_Allreduce(&doRefine, &gRefine, 1, MPI_CXX_BOOL, MPI_LOR, this->m_octDA->getCommActive());

  if (not(gRefine)) {
    return NULL;
  }
  std::vector<ot::TreeNode<DENDRITE_UINT, DIM>> newDATreeNode;
  ot::SFC_Tree<DENDRITE_UINT, DIM>::distRemeshWholeDomain(oldDAtreeNode, refineFlags_, newDATreeNode, surrTreeNode_,
                                                          0.3, MPI_COMM_WORLD);
  newDATreeNode_.resize(newDATreeNode.size());
  std::copy(newDATreeNode.begin(), newDATreeNode.end(), newDATreeNode_.begin());
  DA *newDA = nullptr;//new DA(newDATreeNode, MPI_COMM_WORLD, this->m_octDA->getElementOrder(), 100, 0.3);
  std::swap(newDATreeNode_, oldDAtreeNode);
  return newDA;
}

DA *Refinement::getRefineSubDA(DistTREE &oldDistTree) {
  bool doRefine = true;
  if (std::all_of(refineFlags_.begin(), refineFlags_.end(),
                  [](ot::OCT_FLAGS::Refine v) { return v == ot::OCT_FLAGS::Refine::OCT_NO_CHANGE; })) {
    doRefine = false;
  }

  bool gRefine = false;
  MPI_Allreduce(&doRefine, &gRefine, 1, MPI_CXX_BOOL, MPI_LOR, MPI_COMM_WORLD);

  if (not(gRefine)) {
    return NULL;
  }
  DistTREE::distRemeshSubdomain(oldDistTree, refineFlags_, newDistTree_, surrDistTree_, 0.3);

  DA *newDA = new DA(newDistTree_, MPI_COMM_WORLD, this->m_octDA->getElementOrder(), 100, 0.3);
  std::swap(newDistTree_, oldDistTree);
  return newDA;


}

DA *Refinement::getForceRefineSubDA(DistTREE &oldDistTree) {
  std::fill(refineFlags_.begin(), refineFlags_.end(), ot::OCT_FLAGS::Refine::OCT_NO_CHANGE);
  DistTREE::distRemeshSubdomain(oldDistTree, refineFlags_, newDistTree_, surrDistTree_, 0.3);

  DA *newDA = new DA(newDistTree_, MPI_COMM_WORLD, this->m_octDA->getElementOrder(), 100, 0.3);
  std::swap(newDistTree_,oldDistTree);
  return newDA;


}

void Refinement::initRefinement() {
  this->traverse();
}

void Refinement::petscIntergridTransfer(DA *newDA, const DistTREE &newDistTree, Vec &inVec, const DENDRITE_UINT ndof) {
  DA *surrDA = new DA(surrDistTree_, MPI_COMM_WORLD, newDA->getElementOrder());
  static std::vector<VECType> fineGhosted, surrGhosted;
  newDA->template createVector<VECType>(fineGhosted, false, true, ndof);
  surrDA->template createVector<VECType>(surrGhosted, false, true, ndof);
  std::fill(fineGhosted.begin(), fineGhosted.end(), 0);

  VECType *fineGhostedPtr = fineGhosted.data();
  VECType *surrGhostedPtr = surrGhosted.data();
  PetscScalar *array;
  VecGetArray(inVec, &array);

  ot::distShiftNodes(*this->m_octDA, array,
                     *surrDA, surrGhostedPtr + ndof * surrDA->getLocalNodeBegin(),
                     ndof);

  surrDA->template readFromGhostBegin<VECType>(surrGhostedPtr, ndof);
  surrDA->template readFromGhostEnd<VECType>(surrGhostedPtr, ndof);

  fem::MeshFreeInputContext<VECType, TREENODE>
    inctx{surrGhostedPtr,
          surrDA->getTNCoords(),
          (unsigned) surrDA->getTotalNodalSz(),
          &(*surrDistTree_.getTreePartFiltered().cbegin()),
          surrDistTree_.getTreePartFiltered().size(),
          *surrDA->getTreePartFront(),
          *surrDA->getTreePartBack()};

  fem::MeshFreeOutputContext<VECType, TREENODE>
    outctx{fineGhostedPtr,
           newDA->getTNCoords(),
           (unsigned) newDA->getTotalNodalSz(),
           &(*newDistTree.getTreePartFiltered().cbegin()),
           newDistTree.getTreePartFiltered().size(),
           *newDA->getTreePartFront(),
           *newDA->getTreePartBack()};

  // Hack for the current version
  std::vector<char> outDirty;
  newDA->template createVector<char>(outDirty, false, true, 1);
  newDA->setVectorByScalar(&(*outDirty.begin()), (std::array<char, 1>{0}).data(), false, true, 1);

  const RefElement *refel = newDA->getReferenceElement();
//  fem::locIntergridTransfer(inctx, outctx, ndof, refel, &(*outDirty.begin()));
//
//  newDA->template writeToGhostsBegin<VECType>(fineGhostedPtr, ndof, &(*outDirty.cbegin()));
//  newDA->template writeToGhostsEnd<VECType>(fineGhostedPtr, ndof, false, &(*outDirty.cbegin()));

  delete surrDA;
  VecRestoreArray(inVec, &array);
  VecDestroy(&inVec);
  newDA->petscCreateVector(inVec, false, false, ndof);
  double *newDAVecArray;
  VecGetArray(inVec, &newDAVecArray);
  newDA->template ghostedNodalToNodalVec<VECType>(fineGhostedPtr, newDAVecArray, true, ndof);
  VecRestoreArray(inVec, &newDAVecArray);
}