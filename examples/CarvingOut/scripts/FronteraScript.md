# Frontera script for Fig. 5


```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 4                  # Total # of nodes (varied from 4 to 512)
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

ibrun ${PATH_TO_signedDistance} dragon.stl 4 12 0

python3 signedDistance.py dragon.stl
```