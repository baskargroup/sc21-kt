////
//// Created by maksbh on 4/2/21.
////
//
//


#include "matvec_bench.h"
#include "feMatrix.h"
#include <DendriteUtils.h>
#include <SubDA/SubDomain.h>
#include "../include/Refine.h"
#include <heatMat.h>
#include <heatVec.h>
#include <feMatrix.h>
#include <matvec_bench.h>
#include <IO/VTU.h>
#include <chrono>
namespace bench {
  profiler_t t_adaptive_tsort;
  profiler_t t_adaptive_tconstr;
  profiler_t t_adaptive_tbal;
  profiler_t t_adaptive_oda;

  profiler_t t_ghostexchange;
  profiler_t t_topdown;
  profiler_t t_bottomup;
  profiler_t t_treeinterior;
  profiler_t t_elemental;
  profiler_t t_matvec;

  struct ReportSizes
  {
    unsigned int b1_treeSortSz;
    unsigned int b1_treeConstructionSz;
    unsigned int b1_treeBalancingSz;
    unsigned int b2_treeMatvecSz;

    unsigned int b1_globNodeSz;    // Only used globally.
    unsigned int b2_globNodeSz;    // Only used globally.
  } gRptSz, gDistRptSz;
  void resetAllTimers() {
    t_adaptive_tsort.clear();
    t_adaptive_tconstr.clear();
    t_adaptive_tbal.clear();
    t_adaptive_oda.clear();

    t_ghostexchange.clear();
    t_topdown.clear();
    t_bottomup.clear();
    t_treeinterior.clear();
    t_elemental.clear();
    t_matvec.clear();
  }
    template <int eleOrder>
    void bench_kernel(DA *octDA, const std::vector<TREENODE> &treePart, unsigned int numWarmup, unsigned int numRuns,
                    const DomainExtents &domain, MPI_Comm comm) {
    resetAllTimers();
    {

      gRptSz.b2_treeMatvecSz = treePart.size();
      const unsigned int DOF = 1;   // matvec only supports dof==1 right now.

      std::vector<double> uSolVec, fVec, mfVec, dummyVec;
      octDA->createVector(uSolVec, false, false, DOF);
      /// octDA->createVector(fVec,false,false,DOF);
      /// octDA->createVector(mfVec,false,false,DOF);
      octDA->createVector(dummyVec, false, false, DOF);
      double *uSolVecPtr = &(*(uSolVec.begin()));
      /// double *fVecPtr=&(*(fVec.begin()));
      /// double *mfVecPtr=&(*(mfVec.begin()));
      double *dummyVecPtr = &(*(dummyVec.begin()));


      HeatEq::HeatMat<DIM,eleOrder> heatMat(octDA, &treePart, DOF);
      heatMat.setProblemDimensions(domain.fullDADomain.min, domain.fullDADomain.max);

      /// HeatEq::HeatVec<dim> heatVec(octDA, &treePart,DOF);
      /// heatVec.setProblemDimensions(domain_min,domain_max);

      /// double * ux=octDA->getVecPointerToDof(uSolVecPtr,VAR::M_UI_U, false,false);
      /// double * frhs=octDA->getVecPointerToDof(uSolVecPtr,VAR::M_UI_F, false,false);
      /// double * Mfrhs=octDA->getVecPointerToDof(uSolVecPtr,VAR::M_UI_MF, false,false);
      double *ux = uSolVecPtr;
      /// double *frhs = fVecPtr;
      /// double *Mfrhs = mfVecPtr;
      double *dummy = dummyVecPtr;

      //TODO check if these interface are ready for 4D coordinates.

      /// std::function<void(double,double,double,double*)> f_rhs =[](const double x,const double y,const double z,double* var){
      ///     var[0]=1;
      /// };

      std::function<void(const double *, double *)> f_init = [](const double *xyz, double *var) {
        var[0] = 1;
      };

      octDA->setVectorByFunction(ux, f_init, false, false, DOF);
      /// octDA->setVectorByFunction(Mfrhs,f_init,false,false,DOF);
      /// octDA->setVectorByFunction(frhs,f_rhs,false,false,DOF);
      octDA->setVectorByFunction(dummy, f_init, false, false, DOF);

      /// heatVec.computeVec(frhs,Mfrhs,1.0);

      // Warmup for matvec.
      for (int ii = 0; ii < numWarmup; ii++) {
        heatMat.matVec(ux, dummy, 1.0);
      }

      // Clear the side effect of warmup.
      t_ghostexchange.clear();
      t_topdown.clear();
      t_bottomup.clear();
      t_treeinterior.clear();
      t_elemental.clear();
      t_matvec.clear();

      // Benchmark the matvec.

      for (int ii = 0; ii < numRuns; ii++) {
        heatMat.matVec(ux, dummy, 1.0);
      }


      /// double tol=1e-6;
      /// unsigned int max_iter=1000;
      /// heatMat.cgSolve(ux,Mfrhs,max_iter,tol,0);

      /// const char * vNames[]={"m_uiU","m_uiFrhs","m_uiMFrhs"};
      /// octDA->vecTopvtu(uSolVecPtr,"heatEq",(char**)vNames,false,false,DOF);
      /// octDA->destroyVector(uSolVec);

    }
  }

  void dump_profile_info(std::ostream &fout, const char *msgPrefix, double *params, const char **paramNames,
                         unsigned int numParams, profiler_t *timers, const char **names, unsigned int n,
                         MPI_Comm comm) {

    double stat;
    double * stat_g = new double [3*n];

    int rank, npes;
    MPI_Comm_rank(comm,&rank);
    MPI_Comm_size(comm,&npes);


    for(unsigned int i=0; i<n; i++)
    {
      stat=(timers[i].seconds); /// timers[i].num_calls ;

      par::Mpi_Reduce(&stat,stat_g + 3*i + 0 ,1, MPI_MIN,0,comm);
      par::Mpi_Reduce(&stat,stat_g + 3*i + 1 ,1, MPI_SUM,0,comm);
      par::Mpi_Reduce(&stat,stat_g + 3*i + 2 ,1, MPI_MAX,0,comm);

      stat_g[ 3*i + 1] = stat_g[ 3*i + 1]/(double)npes;

    }

    par::Mpi_Reduce(&gRptSz.b1_treeSortSz,         &gDistRptSz.b1_treeSortSz,         1, MPI_SUM, 0, comm);
    par::Mpi_Reduce(&gRptSz.b1_treeConstructionSz, &gDistRptSz.b1_treeConstructionSz, 1, MPI_SUM, 0, comm);
    par::Mpi_Reduce(&gRptSz.b1_treeBalancingSz,    &gDistRptSz.b1_treeBalancingSz,    1, MPI_SUM, 0, comm);
    par::Mpi_Reduce(&gRptSz.b2_treeMatvecSz,       &gDistRptSz.b2_treeMatvecSz,       1, MPI_SUM, 0, comm);


    if(!rank)
    {
      fout << "msgPrefix\t" << "npes\t";
      for (unsigned int i = 0; i < numParams; i++)
      {
        fout << paramNames[i] << "\t";
      }
      fout << "treeSortSz\t" << "treeConstructionSz\t" << "treeBalancingSz\t" << "constrNumNodes\t"
           << "treeMatvecSz\t" << "matvecNumNodes\t";
      for(unsigned int i=0; i<n; i++)
      {
        fout<<names[i]<<"(min)\t"<<names[i]<<"(mean)\t"<<names[i]<<"(max)\t";
      }

    }

    if(!rank)
      fout<<std::endl;

    if(!rank)
    {
      fout << msgPrefix << "\t";
      fout << npes << "\t";
      for (unsigned int i = 0; i < numParams; i++)
      {
        fout << params[i] << "\t";
      }
      fout << gDistRptSz.b1_treeSortSz << "\t"
           << gDistRptSz.b1_treeConstructionSz << "\t"
           << gDistRptSz.b1_treeBalancingSz << "\t"
           << gDistRptSz.b1_globNodeSz << "\t"
           << gDistRptSz.b2_treeMatvecSz << "\t"
           << gDistRptSz.b2_globNodeSz << "\t";
      for(unsigned int i=0; i<n; i++)
      {
        fout<<stat_g[3*i + 0]<<"\t"<<stat_g[3*i + 1]<<"\t"<<stat_g[3*i+2]<<"\t";
      }
    }
    delete [] stat_g;

    if(!rank)
      fout<<std::endl;

  }

}


int main(int argc, char *argv[]) {

  dendrite_init(argc, argv);
  if(argc < 5){
    TALYFEMLIB::PrintStatus("Usage: ", argv[0], " level boundaryLevel eleOrder filename");
    return EXIT_FAILURE;
  }
  MPI_Barrier(MPI_COMM_WORLD);
  int level = std::atoi(argv[1]);
  int blevel = std::atoi(argv[2]);
  int _eleOrder = std::atoi(argv[3]);
  static constexpr int eleOrder = 2;
  if(eleOrder != _eleOrder){
    std::cout << "Compile by correct eleOrder";
    exit(EXIT_FAILURE);
  }
  std::string filename = argv[4];
  DomainInfo fullDomain;
  fullDomain.max.fill(5.0);
  fullDomain.min.fill(0.0);
  DomainExtents domain(fullDomain, fullDomain);
  SubDomain subDomain(domain);
  DENDRITE_REAL center[DIM]{2.5, 2.5, 2.5};
  VOXEL::Sphere sphere(center, 0.5);
  subDomain.addObject(sphere);
  std::function<ibm::Partition(const double *, double)> functionToRetain = [&](const double *physCoords,
                                                                               double physSize) {
    return (subDomain.functionToRetain(physCoords, physSize));
  };

  MPI_Barrier(MPI_COMM_WORLD);
  std::chrono::high_resolution_clock::time_point t1 =std::chrono::high_resolution_clock::now();
  DistTREE dTree;
  DA *octDA = createSubDA(dTree, functionToRetain, level, eleOrder);
  while (true) {
    Refine refine(octDA, dTree.getTreePartFiltered(), domain, blevel,level);
    DA *newDA = refine.getRefineSubDA(dTree);
    if (newDA == nullptr) {
      break;
    }
    std::swap(newDA, octDA);
    delete newDA;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  std::chrono::high_resolution_clock::time_point t2 =std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
  TALYFEMLIB::PrintStatus("Total time for DA construction = ", time_span.count() );
  TALYFEMLIB::PrintStatus("Number of nodes = ", octDA->getGlobalNodeSz());
//  IO::writeBoundaryElements(octDA,dTree.getTreePartFiltered(),"boundary","bnd",domain);
  const unsigned int numWarmup = 10;
  const unsigned int numRuns = 10;
  MPI_Comm comm = MPI_COMM_WORLD;
  int nProc,rank;
  MPI_Comm_size(comm, &nProc);
  MPI_Comm_rank(comm, &rank);
  enum DAStat: DENDRITE_UINT {
    NUM_NODE = 0,
    NUM_ELEM = 1,
    NUM_RECV = 2,
    NUM_SEND = 3,
    NUM_COMM = 4,
    MAX = 5
  };
  DENDRITE_UINT localStat[DAStat::MAX],globalStatSum[DAStat::MAX],globalStatMax[DAStat::MAX],globalStatMin[DAStat::MAX];

  localStat[NUM_ELEM] = octDA->getLocalElementSz();
  localStat[NUM_NODE] = octDA->getLocalNodalSz();
  localStat[NUM_RECV] = octDA->getTotalRecvSz();
  localStat[NUM_SEND] = octDA->getTotalSendSz();
  localStat[NUM_COMM] = octDA->getTotalSendSz() + octDA->getTotalRecvSz();


  MPI_Reduce(localStat,globalStatSum,DAStat::MAX,MPI_UINT32_T,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce(localStat,globalStatMax,DAStat::MAX,MPI_UINT32_T,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce(localStat,globalStatMin,DAStat::MAX,MPI_UINT32_T,MPI_MIN,0,MPI_COMM_WORLD);
  double globalStatAvg[DAStat::MAX];
  for(int i = 0; i < DAStat::MAX; i++){
    globalStatAvg[i] = globalStatSum[i]*1.0/nProc;
  }
  if(!rank){
    std::ofstream fout(filename);
    fout << globalStatAvg[0] << " " << globalStatAvg[1] << " " << globalStatAvg[2] << " " << globalStatAvg[3] << " " << globalStatAvg[4] << "\n";
    fout << globalStatMax[0] << " " << globalStatMax[1] << " " << globalStatMax[2] << " " << globalStatMax[3] << " " << globalStatMax[4] << "\n";
    fout << globalStatMin[0] << " " << globalStatMin[1] << " " << globalStatMin[2] << " " << globalStatMin[3] << " " << globalStatMin[4] << "\n";
    fout.close();
  }
  {
    std::vector<DENDRITE_UINT> completeStat(DAStat::MAX *nProc);
    MPI_Gather(localStat, DAStat::MAX, MPI_UINT32_T, completeStat.data(), DAStat::MAX, MPI_UINT32_T, 0, MPI_COMM_WORLD);
    if (!rank) {
      std::ofstream fout(filename + "_complete");
      for (int i = 0; i < nProc; i++){
        fout << i << " " << completeStat[i*DAStat::MAX + 0] << " " << completeStat[i*DAStat::MAX + 1] << " " << completeStat[i*DAStat::MAX + 2] << " " << completeStat[i*DAStat::MAX + 3] << " " << completeStat[i*DAStat::MAX + 4] << "\n";
      }
    }
  }
  bench::bench_kernel<eleOrder>(octDA,dTree.getTreePartFiltered(),numWarmup,numRuns,domain,comm);
  TALYFEMLIB::PrintStatus("Number of active rank = ", octDA->getNpesActive());

  const char * param_names[] = {
    "pts_per_core",
    "eleOrder",
  };

  double params[] = {
    (double) 1000,
    (double) eleOrder,
  };


  const char * counter_names[] = {
    "sort",
    "constr",
    "bal",
    "adaptive_oda",

    "matvec",
    "ghostexchange",
    "topdown",
    "bottomup",
    "treeinterior",
    "elemental",
  };

  profiler_t counters[] = {
    bench::t_adaptive_tsort,
    bench::t_adaptive_tconstr,
    bench::t_adaptive_tbal,
    bench::t_adaptive_oda,

    bench::t_matvec,
    bench::t_ghostexchange,
    bench::t_topdown,
    bench::t_bottomup,
    bench::t_treeinterior,
    bench::t_elemental,
  };
  const unsigned int msgPrefixLimit = 64;
  char msgPrefix[2*msgPrefixLimit + 1];
  msgPrefix[0] = '\0';
  msgPrefix[msgPrefixLimit] = '\0';

  std::strncpy(msgPrefix, filename.c_str(), msgPrefixLimit);
  bench::dump_profile_info(std::cout, msgPrefix, params,param_names,2, counters,counter_names,10, comm);

  MPI_Barrier(MPI_COMM_WORLD);
  dendrite_finalize(octDA);


}