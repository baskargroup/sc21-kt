//
// Created by maksbh on 4/2/21.
//

#ifndef DENDRITEKT_REFINE_H
#define DENDRITEKT_REFINE_H

#include <Traversal/Refinement.h>

class Refine: public Refinement{
  const DENDRITE_UINT boundaryLevel;
  const DENDRITE_UINT baseLevel;
public:
  Refine(DA *da,  const std::vector<TREENODE> & treePart, const DomainExtents &domainInfo, const DENDRITE_UINT blevel, const DENDRITE_UINT baseLevel);

  ot::OCT_FLAGS::Refine getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords) override;

};

Refine::Refine(DA *da,  const std::vector<TREENODE> & treePart, const DomainExtents &domainInfo,const DENDRITE_UINT blevel, const DENDRITE_UINT baselevel)
  :Refinement(da,treePart,domainInfo),boundaryLevel(blevel),baseLevel(baselevel){
  this->initRefinement();
}

ot::OCT_FLAGS::Refine Refine::getRefineFlags(TALYFEMLIB::FEMElm &fe, const std::vector<TALYFEMLIB::ZEROPTV> &coords){
  static constexpr DENDRITE_REAL center[DIM]{2.5,2.5,2.5};
  double dist = 0;
  for (int d = 0; d < DIM; d++){
    dist += (coords[0].data()[d] - center[d])*(coords[0].data()[d] - center[d]);
  }
  dist = sqrt(dist);
  int level[5];
  const double distCenter[5]{0.55,0.60,0.8,1.5,2.0};
  for(int i = 0; i < 5; i++){
    level[i] = std::min(boundaryLevel,baseLevel+5-i);
  }
  int currentLevel = this->m_level;
  int sphereLevel = currentLevel;
  for(int i = 0; i < 5; i++){
    if(dist < distCenter[i] and (sphereLevel < level[i])){
      sphereLevel = level[i];
    }
  }
  int reqLevel = std::max(sphereLevel,currentLevel);

  if((this->m_level < reqLevel)){
    return ot::OCT_FLAGS::Refine::OCT_REFINE;
  }
  return ot::OCT_FLAGS::Refine::OCT_NO_CHANGE;
}

#endif //DENDRITEKT_NSREFINE_H
