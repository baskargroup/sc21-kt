Frontera Scripts for scaling result
===================================

## Strong Scaling for Sphere Carved out case (Fig 9)

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 4                  # Total # of nodes (varied from 4 to 512)
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 6 11 1 6_11_1.txt >  log_6_11_1.txt; # For linear
${EXEC} 6 12 2 6_11_2.txt >  log_6_12_1.txt; # For quadratic
```

## Weak Scaling for Channel case (Fig 10)


```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 1                  # Total # of nodes 
#SBATCH --tasks-per-node 28
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 4 9 1 4_9_1.txt >  log_4_9_1.txt; # For linear
${EXEC} 4 9 2 4_9_2.txt >  log_4_9_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 4                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 5 10 1 5_10_1.txt >  log_5_10_1.txt; # For linear
${EXEC} 5 10 2 5_10_2.txt >  log_5_10_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 32                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 6 11 1 6_11_1.txt >  log_6_11_1.txt; # For linear
${EXEC} 6 11 2 6_11_2.txt >  log_6_11_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 256                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 7 12 1 7_12_1.txt >  log_7_12_1.txt; # For linear
${EXEC} 7 12 2 7_12_2.txt >  log_7_12_2.txt; # For quadratic
```

