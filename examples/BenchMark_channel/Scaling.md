Frontera Scripts for scaling result
===================================

## Strong Scaling for Channel case (Fig 7)

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 4                  # Total # of nodes (varied from 4 to 512)
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 10 12 1 10_12_1.txt >  log_10_12_1.txt; # For linear
${EXEC} 10 12 2 10_12_2.txt >  log_10_12_1.txt; # For quadratic
```

## Weak Scaling for Channel case (Fig 8)


```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 1                  # Total # of nodes 
#SBATCH --tasks-per-node 28
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 9 10 1 9_10_1.txt >  log_9_10_1.txt; # For linear
${EXEC} 9 10 2 9_10_2.txt >  log_9_10_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 4                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 10 11 1 10_11_1.txt >  log_10_11_1.txt; # For linear
${EXEC} 10 11 2 10_11_2.txt >  log_10_11_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 32                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 11 12 1 11_12_1.txt >  log_11_12_1.txt; # For linear
${EXEC} 11 12 2 11_12_2.txt >  log_11_12_2.txt; # For quadratic
```

```
!/bin/sh
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 256                 # Total # of nodes 
#SBATCH --tasks-per-node 56
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5
EXEC="ibrun ${Path to MVCChannel}"

${EXEC} 12 13 1 12_13_1.txt >  log_12_13_1.txt; # For linear
${EXEC} 12 13 2 12_13_2.txt >  log_12_13_2.txt; # For quadratic
```