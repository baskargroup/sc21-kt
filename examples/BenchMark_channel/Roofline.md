Roofline plot (Fig 12)
===================================================


```
#SBATCH -J 'scaling'        # Job name
#SBATCH -o log.o%j            # Name of stdout output file
#SBATCH -p development         # Queue (partition) name
#SBATCH -N 1                  # Total # of nodes (varied from 4 to 512)
#SBATCH --tasks-per-node 1
#SBATCH -t 2:00:00           # Run time (hh:mm:ss)
# Other commands must follow all #SBATCH directives...

module load intel/19.0.5 advisor/19.1.1
ibrun -np 1 advixe-cl -collect survey -project-dir ${outputDir}  
           -search-dir src:={Path to examples/BenchMark_channel/src}
           -- ${PATH_TO_MVCChannel}   5 8  1 log.txt

ibrun -np 1 advixe-cl -collect=tripcounts --flop  
            --mark-up-list={Path to examples/BenchMark_channel/src}/benchmark.cpp  
            -project-dir=${outputDir}   -- ${PATH_TO_MVCChannel} 5  8  1 ${outputFile}
```
