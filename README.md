**Citing the software:**

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5168313.svg)](https://doi.org/10.5281/zenodo.5168313)


This document will show how to obtain and compile SC21-kt and its dependencies.

**Cloning the repository**
```bash
git clone bitbucket.org/baskargroup/sc21-kt.git
```

# Required Dependencies

SC21-kt has these required dependencies:

1. A C++ compiler with C++11 support is required. We aim for compatibility with gcc and icc (Intel C++ compiler).

2. [PETSc >= 3.7, upto 3.13.4 tested](https://www.mcs.anl.gov/petsc/), a mature C library of routines for solving partial
differential equations in parallel at large scales. We mostly use it for solving the sparse `Ax=B`
systems that the finite element method generates in parallel.
PETSc uses [MPI](https://en.wikipedia.org/wiki/Message_Passing_Interface) to operate in parallel.

3. [libconfig](http://hyperrealm.github.io/libconfig/), a mature C/C++ library for parsing
config files. Dendrite-kt has built-in utilities for loading simulation parameters from a file,
and this library is what we use to parse that file.
At the moment, these config files are tightly coupled with Dendrite-kt, so this dependency is required.


## Compiling libconfig

Libconfig's build system is [Autotools](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html), which means you'll need to run `./configure` and then `make` to build.

This guide recommends passing ```--prefix=`pwd`/install``` to `./configure`, which will cause `make install` to copy the output files to `[your_libconfig_dir]/install` instead of `/usr`. This way your libconfig install lives completely inside your libconfig folder. This is necessary if you are working on a system where you don't have admin privileges (i.e. an HPC cluster).

```bash
# Download and extract
wget http://hyperrealm.github.io/libconfig/dist/libconfig-1.7.2.tar.gz
tar xvf libconfig-1.7.2.tar.gz
rm libconfig-1.7.2.tar.gz

# Compile and copy output files to libconfig-1.7.2/install
cd libconfig-1.7.2
./configure --prefix=`pwd`/install
make -j8  # compile with 8 threads
make install

# Permanently set $LIBCONFIG_DIR environment variable, which is what TalyFEM uses
# to find your libconfig install. Also set LD_LIBRARY_PATH to include the
# libconfig lib directory to prevent dynamic linking errors with libconfig++.so.
echo "export LIBCONFIG_DIR=`pwd`/install" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$LIBCONFIG_DIR/lib" >> ~/.bashrc
source ~/.bashrc
```



Compilation on Frontera
=========================

#### Module list ###########

```bash
 1) git/2.24.1     2) autotools/1.2  
 3) cmake/3.16.1   4) pmix/3.1.4   
 5) hwloc/1.11.12  6) xalt/2.10.2   
 7) TACC           8) intel/19.0.5   
 9) impi/19.0.9    10) python3/3.7.0  
 11) petsc/3.12    12) advisor/19.1.1 (For Roofline plot)
```

## Modules that need to be loaded on Frontera

```
module load intel/19.0.5 petsc/3.12
```

### Cmake command
```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=mpiicpc -DCMAKE_C_COMPILER=mpiicc -DCMAKE_BUILD_TYPE=Release\
-DCMAKE_CXX_FLAGS="-xCORE-AVX2 -axCORE-AVX512 -O3 -DNDEBUG" -DPETSC_EXECUTABLE_RUNS=Yes -DENABLE_3D=Yes

make MVCChannel MVCSphere signedDistance
```

Running cases
==========================================
* Channel Scaling ([Frontera script](https://bitbucket.org/baskargroup/sc21-kt/src/master/examples/BenchMark_channel/Scaling.md))

```bash
ibrun ${PATH_TO_MVCChannel} ${baseLevel} ${boundaryLevel} ${eleOrder} ${outputFile}
where:
baseLevel      : base refinement level
boundaryLevel  : level on the channel boundaries
eleOrder       : element order 1/2
outputFile     : timing data
Ex: ibrun ${PATH_TO_MVCChannel} 10 12 1 log10_12.out
```

* Sphere Scaling ([Frontera script](https://bitbucket.org/baskargroup/sc21-kt/src/master/examples/BenchMark_sphere/Scaling.md))

```bash
ibrun ${PATH_TO_MVCSphere} ${baseLevel} ${boundaryLevel} ${eleOrder} ${outputFile}
where:
baseLevel      : base refinement level
boundaryLevel  : level on sphere boundaries
eleOrder       : element order 1/2
outputFile     : timing data
Ex: ibrun ${PATH_TO_MVCSphere} 7 12 1 log7_12.out
```

* Signed Distance ([Frontera script](https://bitbucket.org/baskargroup/sc21-kt/src/master/examples/CarvingOut/scripts/FronteraScript.md))

```bash
ibrun ${PATH_TO_signedDistance} ${baseLevel} ${boundaryLevel} 0
where:
baseLevel      : base refinement level
boundaryLevel  : level on stl boundaries

python3 signedDistance.py ${stlFileName}
# signedDistance.py located at examples/CarvingOut/scripts/
Ex: ibrun ${PATH_TO_signedDistance} 4 12 0
```

* Roofline ([Frontera script](https://bitbucket.org/baskargroup/sc21-kt/src/master/examples/BenchMark_channel/Roofline.md))

```bash
ibrun -np 1 advixe-cl -collect survey -project-dir ${outputDir}  
           -search-dir src:={Path to examples/BenchMark_channel/src}
           -- ${PATH_TO_MVCChannel}   ${baseLevel} ${boundaryLevel} ${eleOrder} ${outputFile}

ibrun -np 1 advixe-cl -collect=tripcounts --flop  
            --mark-up-list={Path to examples/BenchMark_channel/src}/benchmark.cpp  
            -project-dir=${outputDir}   -- ${PATH_TO_MVCChannel} ${baseLevel} ${boundaryLevel} ${eleOrder} ${outputFile}
```

Contributors
============
* Kumar Saurabh
* Masado Ishii
* Milinda Fernando
* Hari Sundar
* Baskar Ganapathysubramanian
