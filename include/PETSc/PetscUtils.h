//
// Created by maksbh on 9/18/19.
//

#ifndef DENDRITEKT_PETSCUTILS_H
#define DENDRITEKT_PETSCUTILS_H

#include <PETSc/Solver/LinearSolver.h>
#include <PETSc/Solver/NonLinearSolver.h>
#include <PointCloud/PointData.h>
namespace PETSc {
template<typename Equation, typename NodeData>
LinearSolver *setLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                              ot::DA<DIM> *da,
                              DENDRITE_UINT ndof = 1,
                              bool mfree = true,
                              const bool oneShotAssembly = false) {

  LinearSolver *solver = new LinearSolver(da,talyEq->getMat(),talyEq->getVec(),talyEq->getDomainMin(),talyEq->getDomainMax(),ndof,mfree,oneShotAssembly);
  if (da->isActive()) {
    solver->init();
  }
  return solver;

}

template<typename Equation, typename NodeData>
NonlinearSolver *setNonLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                              ot::DA<DIM> *da,
                              DENDRITE_UINT ndof = 1,
                              bool mfree = true,
                              const bool oneShotAssembly = false) {

  NonlinearSolver * solver = new NonlinearSolver(da,talyEq->getMat(),talyEq->getVec(),talyEq->getDomainMin(),talyEq->getDomainMax(),ndof,mfree,oneShotAssembly);
  if (da->isActive()) {
    solver->init();
  }
  return solver;

}

static void petscDumpFilesforRegressionTest(DA *octDA, const Vec &v, const std::string file_prefix = "vec") {
  PetscViewer viewer;
  int ierr = PetscViewerBinaryOpen(octDA->getCommActive(), file_prefix.c_str(), FILE_MODE_WRITE, &viewer);
  assert(ierr == 0);
  ierr = VecView(v, viewer);
  assert(ierr == 0);
  ierr = PetscViewerDestroy(&viewer);
  assert(ierr == 0);
}

static void setVectorFromPoints(const DA * octDA,const PointCloud::PointData<PointCloud::InputType::CARTESIAN> & pointCloudData,
    const DomainExtents & domain, Vec & vec,const bool isElemental = false,const bool isAllocated = false){
  if(not(isAllocated)){
    octDA->petscCreateVector(vec,isElemental,false,pointCloudData.getDof());
  }
  OctToPhysical octToPhysical(domain);
  double coords[DIM];
  std::function<void(const double *, double *)> functionPointer = [&](const double *x, double *var) {
    std::memcpy(coords,x,sizeof(double)*DIM);
    octToPhysical.convertCoordsToPhys(coords,1);
    pointCloudData.getValue(var,coords);
  };
  octDA->petscSetVectorByFunction(vec,functionPointer,isElemental,false,pointCloudData.getDof());
}

static void recombineVec(DA * da, std::vector<VecInfo> & inVecs, VecInfo & outVec, bool isAllocated = false){
    DENDRITE_UINT ndof = 0;
    for(auto & vec:inVecs){
        ndof += vec.ndof;
        VecGetArrayRead(vec.v,&vec.val);
    }

    if(not(isAllocated)){
        da->petscCreateVector(outVec.v,false,false,ndof);
    }
    PetscScalar * syncArray;
    VecGetArray(outVec.v,&syncArray);
    DENDRITE_UINT  currentDof(0) ;
    const DENDRITE_UINT localNodalSz = da->getLocalNodalSz();
    for(DENDRITE_UINT node = 0; node < localNodalSz; node++) {
        currentDof = 0;
        for (const auto&vec:inVecs) {
            std::memcpy(&syncArray[node*ndof + currentDof], &vec.val[node*vec.ndof], sizeof(PetscScalar)*vec.ndof);
            currentDof += vec.ndof;
        }
    }

    for(auto & vec:inVecs){
        VecRestoreArrayRead(vec.v,&vec.val);
    }
    VecRestoreArray(outVec.v,&syncArray);
    outVec.ndof = ndof;
}
}

#endif //DENDRITEKT_PETSCUTILS_H
